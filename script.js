/*
  Реалізувати функцію створення об'єкта "юзер". 
  Завдання має бути виконане на чистому Javascript без використання бібліотек типу jQuery або React.

Технічні вимоги:

- Написати функцію createNewUser(), яка буде створювати та повертати об'єкт newUser.
- При виклику функція повинна запитати ім'я та прізвище.
- Використовуючи дані, введені юзером, створити об'єкт newUser з властивостями firstName та lastName.
- Додати в об'єкт newUser метод getLogin(), який повертатиме першу літеру імені юзера, з'єднану з прізвищем, 
  все в нижньому регістрі (наприклад, Ivan Kravchenko → ikravchenko).
- Створити юзера за допомогою функції createNewUser(). 
  Викликати у цього юзера функцію getLogin(). Вивести у консоль результат виконання функції.

Необов'язкове завдання підвищеної складності

- Зробити так, щоб властивості firstName та lastName не можна було змінювати напряму. 
  Створити функції-сеттери setFirstName() та setLastName(), які дозволять змінити дані властивості.
*/
"use strict";
const getUserNameWithPrompt = function () {
  let firstName = "";
  let lastName = "";

  do {
    firstName = prompt("Input First Name", firstName) ?? "";
    lastName = prompt("Input Last Name", lastName) ?? "";
  } while (!firstName.trim() || !lastName.trim());
  return { firstName, lastName };
};

const createNewUser = function () {
  const newUser = getUserNameWithPrompt();
  Object.defineProperty(newUser, "firstName", {
    writable: false,
  });
  Object.defineProperty(newUser, "lastName", {
    writable: false,
  });
  newUser.setLastName = function (lastName) {
    Object.defineProperty(this, "lastName", { value: lastName });
  };
  newUser.setFirstName = function (firstName) {
    Object.defineProperty(this, "firstName", { value: firstName });
  };
  newUser.getLogin = function () {
    return (this.firstName[0] + this.lastName).toLowerCase();
  };
  return newUser;
};

const newUser = createNewUser();
console.log(newUser);
console.log(newUser.getLogin());
